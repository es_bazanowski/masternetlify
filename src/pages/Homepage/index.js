import Navbar from '../../components/navbar';
import AboutMainWrapper from '../../components/aboutme';
import Tools from '../../components/tools';
import MyWorks from '../../components/myworks';
import Blog from '../../components/blog';
import ContactMe from '../../components/contact';
import Circle3 from '../../pic/koła_03.png';
import Circle2 from '../../pic/koła_02.png';
import Circle1 from '../../pic/koła_01.png';
const Homepage = () => {

    return (
        <div class="main_content_wrapper">
            <div id="background_circle_1"><img id="circle1" src={Circle3} /></div>
            <div id="background_circle_2"><img id="circle2" src={Circle1} /></div>
            <div id="background_circle_3"><img id="circle3" src={Circle2} /></div>
            <div id="background_circle_4"><img id="circle4" src={Circle3} /></div>
            <div id="background_circle_5"><img id="circle5" src={Circle1} /></div>
            <div id="background_circle_6"><img id="circle6" src={Circle1} /></div>
            <div id="background_circle_7"><img id="circle7" src={Circle3} /></div>
            <Navbar />
            <AboutMainWrapper />
            <Tools />
            <MyWorks />
            <Blog />
            <ContactMe />
        </div>
    )

}
export default Homepage;