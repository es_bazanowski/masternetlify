import About1 from './about1';
import About2 from './about2';
const AboutMainWrapper= () => {
    return (
        <div class="main-about_wrapper">
            <About1 />
            <About2 />

        </div>
    )
}

export default AboutMainWrapper;