import bitbucket from '../../pic/bit_bucket_icon.png';
import devicon from '../../pic/dev_icon.png';
import easycode from '../../pic/easy_code_button.png';
const About1 = () => {
    return (
        <div>
        <div class="about-content_wrapper1">
        <div class="picme_wrapper">
            <img id="pic_me"
                src="https://images1.westend61.de/0000388976pw/portrait-of-tattooed-brunette-man-in-front-of-gray-background-STKF000953.jpg"/>
            <div class="about-cloud_wrapper">
                <span id="myname">//Hi, My name is Sebastian Bazanowski</span><br></br>
                <span id="specialization">Software Engineer</span><br></br><br></br>
                <span id="experience">Passionate Techy and Tech Author</span><br></br>
                <span id="experience">with 3 years of experience within the field.</span><br></br><br></br><br></br>
                <span id="seemyworks">See my works</span>
                <div id="icons_wrapper"><img class="cloud-icons" src={bitbucket}/><img
                        class="cloud-icons" src={devicon}/></div>
            </div>
        </div>


        <div class="allabout-content_wrapper">

            <span class="title-aboutme">//About me</span><br></br>
            <span class="over-under-about">All about Techy</span><br></br><br></br><br></br>

            <span class="about-lorem">Lorem ipsum dolor sit amet,<br></br><br></br>
                consectetur adipiscing elit, sed do eiusmod tempor<br></br>
                incididunt ut labore et dolore magna aliqua. Ut enim ad<br></br>
                minim veniam, quis nostrud exercitation ullamco laboris nisi<br></br>
                ut aliquip ex ea commodo consequat.<br></br><br></br>
                Duis aute irure dolor in reprehenderit in voluptate velit esse<br></br>>
                cillum dolore eu fugiat nulla pariatur.<br></br>

            </span><br></br>

            <span class="over-under-about">My interests</span><br></br><br></br>

            <span class="list">· music</span><br></br>
            <span class="list">· reading</span><br></br>
            <span class="list">· football</span>
           
            <div class="easycode_wrapper">
                <span id="easycode">Ukończyłem kurs Easy Code</span>
                <div class="easypic"><img id="code" src={easycode}/>
                </div>
            </div>
        </div>
    </div>
    </div>
    )
}

export default About1;