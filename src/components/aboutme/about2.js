import kontakticon from '../../pic/kontakt_ikona.png';


const About2 = () => {
    return (
        <div class="about-content_wrapper2">

                <div id="skills" className="skills_wrapper">
                    <span className="title-skills">//Skills</span><br></br><br></br>
                    <span className="title-content">Duis aute irure dolor in reprehenderit in voluptate
                        velit<br></br>
                        esse cillum dolore eu fugiat nulla pariatur.</span><br></br><br></br><br></br><br></br>
                    <div id="back-1">
                        <div className="advanced_wrapper1">
                            <div id="php">PHP 100%</div>
                        </div>
                    </div>
                    <div id="back-2">
                        <div className="advanced_wrapper2">
                            <div id="js">JS 90%</div>
                        </div>
                    </div>
                    <div id="back-3">
                        <div className="advanced_wrapper3">
                            <div id="html">HTML 90%</div>
                        </div>
                    </div>
                    <div id="back-4">
                        <div className="advanced_wrapper4">
                            <div id="nodejs">NODEJS 60%</div>
                        </div>
                    </div>
                    <div id="back-5">
                        <div className="advanced_wrapper5">
                            <div id="css">CSS 90%</div>
                        </div>
                    </div>
                    <div id="back-6">
                        <div className="advanced_wrapper6">
                            <div id="go">GO 60%</div>
                        </div>
                    </div>
                </div>
                <div className="main-buttons_wrapper">
                    <span id="freelancer">//I am a freelancer</span>
                    <span id="under-freelancer"> Contact me if you want to work with me</span><br></br>
                    <div className="buttons-content_wrapper">
                        <img id="hireme" src={kontakticon}/>
                        <img id="cv" src={kontakticon}/>
                        <div className="onbutton-text_wrapper1">
                            <span>Hire me</span>
                        </div>
                        <div className="onbutton-text_wrapper2">
                            <span>Download CV</span>
                        </div>
                    </div>
                </div>
                </div>

    )
}

export default About2;