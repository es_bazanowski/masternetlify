import Circle3 from '../../pic/koła_03.png';
import ContactIcon from '../../pic/kontakt_ikona.png';
import ContactIc from '../../pic/contact.png';

const ContactMe = () => {
    return (
        <div class="main-contact_wrapper">
        <div id="background_circle_9"><img id="circle9" src={Circle3}/></div>
        <div id="Contact_me" class="contact-content_wrapper1">
            <span id="contact_title">//Contact me</span><br></br><br></br>
            <span id="contact_description">If you are willing to work with me, contact me. I can join your<br></br>
                conference to serve you with my engeneering experience. </span><br></br><br></br>

            <form action="*">
                <input type="text" name="email" placeholder=" Your e-mail" /><br></br>
                <input type="text" name="name" placeholder=" Your name" /><br></br>
                <textarea rows="12" cols="50" name="texarea"
                    placeholder="How can I help you? Please, put here your message/request."></textarea>
            </form>
            <div class="send_button">
                <img id="send_button_pic" src={ContactIcon}/>
                <div class="send-button-text">
                    <span>Send</span>
                </div>
            </div>
            <div class="right-side-contact-wrapper">
                <div class="contact-cloud_wrapper">
                    <img id="contact-icon" src={ContactIc}/>
                    <span>jonh_doe@gmail.com<br></br><br></br>+ 32 123 456 567</span>
                </div>
                <div class="contact-picutre_wrapper">
                    <img id="contact-pic"
                        src="https://images1.westend61.de/0000388976pw/portrait-of-tattooed-brunette-man-in-front-of-gray-background-STKF000953.jpg"/>
                </div>
                <div class="contact-text-wrapper">
                    autor: John Doe<br></br>
                    username: @johndoe<br></br>
                    description: University Graduate <br id="new-line-br-for-mobile"></br> Software Engineer<br></br>
                    homepage: johndoe.github.pl<br></br>
                    repository type: Open-source<br></br>
                    url: github.com/johndoe<br></br>
                </div>
            </div>
        </div>

    </div>
    )
}
export default ContactMe;