import BitBucketIcon from '../../pic/bit_bucket_icon.png';
import ExternalIcon from '../../pic/external_link_icon.png';

const MyWorks = () => {
    return (
        <div class="myworks-main_wrapper">
            <div class="myworks-content-wrapper1">
                <span id="myworks">//My works<br></br></span><span id="portfolio">Portfolio<br></br><br></br></span>
                <span id="portfolio-content">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris<br></br>
                    nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in<br></br>
                    reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                    pariatur.</span>
            </div>
            <div class="myworks-content-wrapper2">
                <div class="picelement">
                    <div class="background-hover">
                        <div class="background-text">
                            <span id="title-picelement">Office Software</span><br></br>
                            <span id="task">- task management</span>

                            <p id="picelement-tools">Tools: React/Redux</p>
                        </div>
                    </div>

                    <img
                        src="https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/97f43257974261.59eaa1b771bc6.png"/>
                    <div class="portfolio-icons">
                        <img src={BitBucketIcon}/>
                        <img src={ExternalIcon}/>
                    </div>
                </div>
                <div class="picelement">
                    <div class="background-hover">
                        <div class="background-text">
                            <span id="title-picelement">Office Software</span><br></br>
                            <span id="task">- task management</span>

                            <p id="picelement-tools">Tools: React/Redux</p>
                        </div>
                    </div>
                    <img src="https://gomez.pl/assets/photo/upload/categories/sale-scale-1200-0.jpg"/>
                    <div class="portfolio-icons">
                        <img src={BitBucketIcon}/>
                        <img src={ExternalIcon}/>
                    </div>
                </div>
                <div class="picelement">
                    <div class="background-hover">
                        <div class="background-text">
                            <span id="title-picelement">Office Software</span><br></br>
                            <span id="task">- task management</span>

                            <p id="picelement-tools">Tools: React/Redux</p>
                        </div>
                    </div>
                    <img src="https://assets.landingi.com/wp-content/uploads/2020/07/06144837/landing-pages-14-12.png"/>
                    <div class="portfolio-icons">
                        <img src={BitBucketIcon}/>
                        <img src={ExternalIcon}/>
                    </div>
                </div>
                <div class="picelement">
                    <div class="background-hover">
                        <div class="background-text">
                            <span id="title-picelement">Office Software</span><br></br>
                            <span id="task">- task management</span>

                            <p id="picelement-tools">Tools: React/Redux</p>
                        </div>
                    </div>
                    <img src="https://www.heuristic.pl/img/realizacje/nowe/_orginal/landing-page-mio1.jpg"/>
                    <div class="portfolio-icons">
                        <img src={BitBucketIcon}/>
                        <img src={ExternalIcon}/>
                    </div>
                </div>
                <div class="picelement">
                    <div class="background-hover">
                        <div class="background-text">
                            <span id="title-picelement">Office Software</span><br></br>
                            <span id="task">- task management</span>

                            <p id="picelement-tools">Tools: React/Redux</p>
                        </div>
                    </div>
                    <img src="https://www.orsodesign.pl/assets/img/portfolio/strona_ttmagic1.jpg"/>
                    <div class="portfolio-icons">
                        <img src={BitBucketIcon}/>
                        <img src={ExternalIcon}/>
                    </div>
                </div>
                <div picelement class="picelement">
                    <div class="background-hover">
                        <div class="background-text">
                            <span id="title-picelement">Office Software</span><br></br>
                            <span id="task">- task management</span>

                            <p id="picelement-tools">Tools: React/Redux</p>
                        </div>
                    </div>
                    <img src="https://www.nda.ac.uk/wp-content/uploads/10-10-portfolio-03-1.jpg"/>
                    <div class="portfolio-icons">
                        <img src={BitBucketIcon}/>
                        <img src={ExternalIcon}/>
                    </div>
                    <div id="background-opacity-cloud"></div>
                </div>
            </div>
        </div>
    )
}
export default MyWorks;