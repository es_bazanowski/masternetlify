import menugradient from '../../pic/menu_gradient.png';
import twitter from '../../pic/twitter.png';
import facebook from '../../pic/facebook.png';
import linkedin from '../../pic/linkedin.png';

const Navbar = () => {
    return (
        
        <div class="main-navbar_wrapper">
            <div className="first-navbar_wrapper">
                <span>S.B.</span>
                <div className="dropdown">
                    <button className="dropbtn">|||</button>
                    <div className="dropdown-content">
                        <a href="#aboutme">About me</a>
                        <a href="#skills">Skills</a>
                        <a href="#portfolio">Portfolio</a>
                        <a href="#blog">Blog</a>
                        <a href="#Contact_me">Contact me</a>
                        <a href="CV">Download CV</a>
                    </div>
                </div>

            </div>

            <div className="second-navbar_wrapper">

                <img id="menu_pic" src={menugradient}/>
                    <div className="navbar-list_wrapper">
                     
                        <span><a href="#aboutme">About me</a></span>
                        <span><a href="#skills">Skills</a></span>
                        <span><a href="#portfolio">Portfolio</a></span>
                        <span><a href="#blog">Blog</a></span>
                        <span id="before-line"><a href="#Contact_me">Contact me</a></span>
                        <div id="navbar-line"></div>
                        <span id="after-line"><img id="twitter" src={twitter} /></span>
                        <span><img id="fb" src={facebook} /></span>
                        <span id="linkedin"><img src={linkedin} /></span>
                      
                    </div>
            </div>
          </div>
          

    )

}
export default Navbar;