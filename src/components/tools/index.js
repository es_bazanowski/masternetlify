
import programIcon from '../../pic/tool_program_ikona.png';

const Tools = () => {
    return (
        <div class="main-tools_wrapper">
            <div class="tools-content_wrapper1">
                <span id="tools">//Tools</span><br></br>
                <span id="essentials">My essentials</span>
            </div>
            <div class="tools-content_wrapper2">
                <div class="essential-icons">
                    <img src="https://www.mtps-aufbereitung.de/wp-content/uploads/2016/03/comsecura-mtps.png"/>
                    <div class="title-to-icons">
                        <span>React<br></br>
                            16.6.3</span>
                    </div>
                </div>
                <div class="essential-icons">
                    <img id="webpack" src="https://cdn.iconscout.com/icon/free/png-512/webpack-1-1174980.png"/>
                    <div class="title-to-icons">
                        <span>Webpack<br></br>
                            4.19.1</span>
                    </div>
                </div>
                <div class="essential-icons">
                    <img src="https://cdn.iconscout.com/icon/free/png-256/node-js-1174925.png"/>
                    <div class="title-to-icons">
                        <span>Express<br></br>
                            4.16.4</span>
                    </div>
                </div>
                <div class="essential-icons">
                    <img id="styledcom"
                        src="https://images.ctfassets.net/qcrphhesuv4n/6cZj9wicrfq7gQwyYy7RcI/9ec870532475e09f72bb1c7143a4564b/1_p1TndLk3UsGPBsM7qHPZIw.png?w=800&q=50"/>
                    <div class="title-to-icons">
                        <span>Styled<br></br>Components<br></br>
                            4.16.4</span>
                    </div>
                </div>
                <div class="essential-icons">
                    <img src="https://cdn.iconscout.com/icon/free/png-512/redux-283024.png"/>
                    <div class="title-to-icons">
                        <span>Redux<br></br>
                            4.0.1</span>
                    </div>
                </div>
                <div class="essential-icons">
                    <img src="https://cdn4.iconfinder.com/data/icons/iconsimple-programming/512/css-512.png"/>
                    <div class="title-to-icons">
                        <span>Flexbox<br></br>
                            4.0.1</span>
                    </div>
                </div>
                <div class="essential-icons">
                    <img src={programIcon}/>
                    <div class="title-to-icons">
                        <span>Program<br></br>
                            5.2.1</span>
                    </div>
                </div>
                <div class="essential-icons">
                    <img src={programIcon}/>
                    <div class="title-to-icons">
                        <span>Program<br></br>
                            5.2.2</span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Tools;