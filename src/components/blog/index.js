
const Blog = () => {
    return (
        <div id="blog" class="main-blog_wrapper">
        <div id="background_circle_8"><img id="circle8" src="pic/koła_01.png"/></div>
        <div class="main-blog-content_wrapper">
            <span id="blog-title">//Blog posts</span><br></br>
            <span id="blog-description">Hints and tips</span>
        </div>
        <div class="blog-content_wrapper1">
            <img
                src="https://soundartifacts.com/images/how-to/direct-download-ms-office-2016-ms-office-2019-and-ms-office-365-iso.jpg"/>
            <div class="title1-wrapper">
                <span id="blogtitle1">//Title 01</span><span class="title1-data1">s.bazanowski
                    25.11.2020</span><br></br>
                <span id="title-description1">Secondary Title</span><br></br><br></br>
                <span>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                    ea
                    commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum
                    dolore eu fugiat nulla pariatur.</span>
                <span class="title1-readmore">Read more ></span>

            </div>
        </div>
        <div class="blog-content_wrapper1">
            <img src="https://www.komputronik.pl/informacje/wp-content/uploads/2020/04/microsoft-365-1.jpg"/>
            <div class="title1-wrapper">
                <span id="blogtitle2">//Title 02</span><span class="title1-data1">s.bazanowski
                    25.11.2020</span><br></br>
                <span id="title-description2">Secondary Title</span><br></br><br></br>
                <span>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                    ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                    dolore eu fugiat nulla pariatur.</span>
                <span class="title1-readmore">Read more > </span>

            </div>
        </div>
    </div>
    )
}
export default Blog;